import express, { Router } from "express";
import {
  getProdcutById,
  getProdcutReviews,
  getProdcutOffers,
  searchProdcutByName,
} from "../controller/ProductController.js";

const router = Router();
const users = [
  { id: 1, name: "alaa" },
  { id: 2, name: "asaad" },
];

router.get("/users", (req, res, next) => {
  console.log("user route");
  res.status(200).send(users);
});
router.get("/product/:productID", getProdcutById);
router.get("/product/:productID/reviews", getProdcutReviews);
router.get("/product/:productID/offers", getProdcutOffers);
router.get("/search/:searchQuery", searchProdcutByName);

export default router;
