import express from "express";
import router from "./routes/router.js";

const app = express();
app.use(express.json());
console.log(process.env.API_KEY);

app.use(router);

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
  console.log(`running Server on port ${PORT}`);
});
