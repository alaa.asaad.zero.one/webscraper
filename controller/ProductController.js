import request from "request-promise";

const generateUrl = (apikey) =>
  `http://api.scraperapi.com?api_key=${apikey}&autoparse=true`;

export const getProdcutById = async (req, res, next) => {
  const { productID } = req.params;
  const { apikey } = req.query;
  try {
    const response = await request(
      `${generateUrl(apikey)}&url=https://www.amazon.com/dp/${productID}`
    );
    const data = JSON.parse(response);
    res.send(data);
  } catch (error) {
    res.send(error);
  }
};

export const getProdcutReviews = async (req, res, next) => {
  const { productID } = req.params;
  const { apikey } = req.query;
  try {
    const response = await request(
      `${generateUrl(
        apikey
      )}&url=https://www.amazon.com/product-reviews/${productID}`
    );
    const data = JSON.parse(response);
    res.send(data);
  } catch (error) {
    res.send(error);
  }
};

export const getProdcutOffers = async (req, res, next) => {
  const { productID } = req.params;
  const { apikey } = req.query;
  try {
    const response = await request(
      `${generateUrl(
        apikey
      )}&url=https://www.amazon.com/gp/offer-listing/${productID}`
    );
    const data = JSON.parse(response);
    res.send(data);
  } catch (error) {
    res.send(error);
  }
};

export const searchProdcutByName = async (req, res, next) => {
  const { searchQuery } = req.params;
  const { apikey } = req.query;
  try {
    const response = await request(
      `${generateUrl(apikey)}&url=https://www.amazon.com/s?k=${searchQuery}`
    );
    const data = JSON.parse(response);
    res.send(data);
  } catch (error) {
    res.send(error);
  }
};
